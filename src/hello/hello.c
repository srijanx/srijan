/*
 * srijan/src/hello/main.c
 *
 * Sample application to toggle LED on the Launchpad - based on the
 * "MSP430 Blink the LED Demo" in the MSP430 GCC Linux installer from
 * Texas Instruments.
 *
 * It is included in this package to illustrate the build process.
 *
 *
 * The MIT License
 *
 * Copyright (c) 2014 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <msp430.h>

#define COUNTDOWN	50000

int main(void) {

	volatile unsigned int i;

	/*
	 * Stop watchdog timer
	 */
	WDTCTL = WDTPW | WDTHOLD;

	/*
	 * Set P1.0 as output
	 */
	P1DIR |= 0x01;

	P1OUT = 0;

	for(;;) {
		/*
		 * Toggle P1.0
		 */
		P1OUT ^= 0x01;

		/*
		 * Implement busy delay loop
		 */
		i = COUNTDOWN;

		while (i != 0) {
			i--;
		}
	}

	return 0;
}
