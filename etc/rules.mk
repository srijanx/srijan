#
# srijan/etc/rules.sh
#
# Rules to build and clean application projects.
#
#
# The MIT License
#
# Copyright (c) 2014 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ------------------------------------------------------------------------------
# Set default goal
# ------------------------------------------------------------------------------
.DEFAULT_GOAL	:= all


# ------------------------------------------------------------------------------
# Identify base of this package
#
# This file is included at end of each individual makefile.
# Hence, it is used as a reference to derive the base of this package.
# ------------------------------------------------------------------------------

SRIJAN		:= $(realpath $(dir $(lastword $(MAKEFILE_LIST)))/..)


# ------------------------------------------------------------------------------
# Show banner
# ------------------------------------------------------------------------------

SRIJAN_AUTHOR	:= Sanjeev Premi
SRIJAN_VERSION	:= 0.80
SRIJAN_CONTACT	:= spremi@ymail.com
SRIJAN_LICENSE	:= The MIT License

$(info )
$(info :::)
$(info ::: srijan v$(SRIJAN_VERSION). (Available under $(SRIJAN_LICENSE)).)
$(info :::)
$(info ::: (c) 2014 $(SRIJAN_AUTHOR) ($(SRIJAN_CONTACT)).)
$(info :::)
$(info )


# ------------------------------------------------------------------------------
# Set level of verbosity
# (Silent by default)
# ------------------------------------------------------------------------------

#
# Prefix to hide commands that user is likely to view quite often.
#
Q := @

#
# Prefix to hide commands that user is likely to review rarely.
#
H := @

#
# Adjust verbosity based on command-line flag 'V'
#
ifeq ($(V),1)
Q :=
endif

ifeq ($(V),2)
Q :=
H :=
endif

include $(SRIJAN)/etc/local.mk


# ------------------------------------------------------------------------------
# Perform basic checks
# ------------------------------------------------------------------------------

#
# Check - toolchain has been defined.
#
ifeq ($(TC_NAME),)
$(info )
$(info [E] No toolchain selected.)
$(info [E])
$(info [E] Select a toolchain in the file 'etc/local.mk'.)
$(info [E] Also ensure that install path is also valid.)
$(info )
$(error Unable to proceed with build)
else
$(info ::: Toolchain   : $(TC_NAME))

TC_DEFS	:= $(SRIJAN)/etc/tc-$(TC_NAME).mk
endif

#
# Check - toolchain is supported.
#
TC_DEFS_EXIST := $(shell test -f $(TC_DEFS) && echo 1 || echo 0)

ifeq ($(TC_DEFS_EXIST),0)
$(info )
$(info [E] No definitions exist for selected toolchain.)
$(info [E] Check if the name of the toolchain was misspelt.)
$(info [E])
$(info [E] If you are using a different toolchain, provide)
$(info [E] definitions in the file 'etc/tc-$(TC_NAME).mk')
$(info [E] Use an existing file as template.)
$(info )
$(error Unable to proceed with build)
else
include $(TC_DEFS)

$(info ::: Version     : v$(TC_VERSION) ($(TC_MACHINE)))
$(info :::)
endif


$(info ::: Project     : $(PROJECT))
$(info :::)

#
# Check - MCU has been selected for this build.
#
ifeq ($(MCU),)
$(info )
$(info [E] No MCU has been selected for this project.)
$(info [E])
$(info [E] It can be defined in either of following ways:)
$(info [E]  - In the Makefile for this project.)
$(info [E]  - In the file 'etc/local.mk'.)
$(info [E]  - As environment variable in the shell.)
$(info [E]  - On the command-line as MCU=mcu-name)
$(info )
$(error Unable to proceed with build)
else
$(info ::: MCU         : $(MCU))
endif

$(info )

PATH := $(PATH):$(TC_BIN)


# ------------------------------------------------------------------------------
# Common toolchain options
# ------------------------------------------------------------------------------

#
# Toolchain definitions
#
SRIJAN_AS	:= $(TC_AS)
SRIJAN_CC	:= $(TC_CC)
SRIJAN_AR	:= $(TC_AR)
SRIJAN_LD	:= $(TC_LD)
SRIJAN_NM	:= $(TC_NM)
SRIJAN_OBJDUMP	:= $(TC_OBJDUMP)
SRIJAN_OBJCOPY	:= $(TC_OBJCOPY)
SRIJAN_READELF	:= $(TC_READELF)
SRIJAN_SIZE	:= $(TC_SIZE)

#
# For the compiler
#
SRIJAN_CC_OPTS		= -c -Os -mmcu=$(MCU) $(TC_CC_OPTS) -Wall $(PROJ_INCPATH)

#
# For the assembler
#
SRIJAN_AS_OPTS		= -mmcu=$(MCU) $(TC_CC_OPTS) -Wall

#
# For the linker
#
SRIJAN_LD_OPTS		=  -mmcu=$(MCU) $(TC_LD_OPTS) -Wl,--sort-common
SRIJAN_LD_OPTS		+= -Wl,-Map=$(PROJ_MAP) $(PROJ_LIBPATH)

#
# For the size tool
#
SRIJAN_SIZE_OPTS	=

#
# For the objdump tool
#
SRIJAN_ODUMP_OPTS	= --disassemble --source --headers --line-numbers

#
# For the objcopy tool
#
SRIJAN_OCOPY_OPTS	= --output-target=ihex


# ------------------------------------------------------------------------------
# Derived definitions
# ------------------------------------------------------------------------------

#
# Define 'comma'
#
COMMA		= ,

#
# Name of map file
#
PROJ_MAP	:= $(PROJECT).map

#
# Name of list file
#
PROJ_LST	:= $(PROJECT).lst

#
# Name of executable in ELF format
#
PROJ_OUT	:= $(PROJECT).out

#
# Name of output file in Intel HEX format
#
PROJ_BIN	:= $(PROJECT).bin

#
# Name of archive file
#
PROJ_LIB	:= lib$(PROJECT).a

#
# List of object files
#
PROJ_OBJS	= $(subst .s,.o,$(subst .c,.o,$(SOURCES)))

#
# Search path for headers
#
PROJ_INCPATH	= $(addprefix -I ,$(TC_INCPATH) $(INCPATH))

#
# Search path for libraries
#
PROJ_LIBPATH	= $(addprefix -Wl$(COMMA)-L ,$(TC_LIBPATH) $(LIBPATH))

#
# List of libraries to be passed to the linker.
#
PROJ_LIBS	= $(addprefix -Wl$(COMMA)-l,$(LIBS))


# ------------------------------------------------------------------------------
# Override built-in implicit rules
# ------------------------------------------------------------------------------

%.o : %.c
	$(H)echo "  :"
	$(H)echo "  : Compiling - $(<)"
	$(H)echo "  :"
	$(Q)$(SRIJAN_CC) $(SRIJAN_CC_OPTS) -o $(@) $(<)

%.o : %.s
	$(H)echo "  :"
	$(H)echo "  : Assembling - $(<)"
	$(H)echo "  :"
	$(Q)$(SRIJAN_AS) $(SRIJAN_AS_OPTS) -o $(@) $(<)


# ------------------------------------------------------------------------------
# Internal targets
# ------------------------------------------------------------------------------

-banner-build:
	$(H)echo " ::"
	$(H)echo " :: Building $(PROJECT).$(TARGET)"
	$(H)echo " ::"

-banner-clean:
	$(H)echo " ::"
	$(H)echo " :: Cleaning"
	$(H)echo " ::"

# ------------------------------------------------------------------------------
# Targets
# ------------------------------------------------------------------------------

#
# Build library archive
#
$(PROJ_LIB): -banner-build $(PROJ_OBJS)
	$(H)echo "  :"
	$(H)echo "  : Archive - $(@)"
	$(H)echo "  :"
	$(Q)$(SRIJAN_AR) cr $(@) $(PROJ_OBJS)
	$(H)echo ""
	$(H)ls -lgG $(@)
	$(H)echo ""

.PHONY:	$(PROJECT).lib
$(PROJECT).lib: $(PROJ_LIB)


#
# Build executable
#
$(PROJ_OUT): -banner-build $(PROJ_OBJS)
	$(H)echo "  :"
	$(H)echo "  : Linking - $(@)"
	$(H)echo "  :"
	$(Q)$(SRIJAN_LD) $(SRIJAN_LD_OPTS) $(PROJ_OBJS) $(PROJ_LIBS) $(TC_LD_LIBS)  -o $(@)

	$(H)echo ""
	$(Q)$(SRIJAN_SIZE) $(SRIJAN_SIZE_OPTS) $(@)
	$(H)echo ""

	$(H)echo "  :"
	$(H)echo "  : Creating list file - $(PROJ_LST)"
	$(H)echo "  :"
	$(Q)$(SRIJAN_OBJDUMP) $(SRIJAN_ODUMP_OPTS) $(@) > $(PROJ_LST)


#
# Generate binary in Intel HEX format.
#
$(PROJ_BIN): -banner-build $(PROJ_OUT)
	$(H)echo "  :"
	$(H)echo "  : Creating binary - $(@)"
	$(H)echo "  :"
	$(Q)$(SRIJAN_OBJCOPY) $(SRIJAN_OCOPY_OPTS) $(PROJ_OUT) $(@)
	$(H)echo ""
	$(H)ls -lgG $(PROJ_OUT) $(@)
	$(H)echo ""


#
# Clean project
#
clean: -banner-clean
	$(Q)-rm -f $(PROJ_OBJS)
	$(Q)-rm -f $(PROJ_LST)
	$(Q)-rm -f $(PROJ_MAP)
	$(Q)-rm -f $(PROJ_LIB)
	$(Q)-rm -f $(PROJ_OUT)
	$(Q)-rm -f $(PROJ_BIN)
	$(H)echo " ::"
	$(H)echo " :: Done."
	$(H)echo " ::"
	$(H)echo ""


#
# Default target
#
.PHONY:	all
all:	$(PROJECT).$(TARGET)
	$(H)echo " ::"
	$(H)echo " :: Done."
	$(H)echo " ::"
	$(H)echo ""


#
# Flash the application binary
#
.PHONY:	flash
flash:	$(PROJ_BIN)
	$(Q)mspdebug rf2500 " prog ./$(PROJ_BIN)"
