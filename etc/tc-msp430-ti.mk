#
# srijan/etc/tc-msp430-ti.sh
#
# Definitions specific to MSP430 GCC from TI.
#
#
# The MIT License
#
# Copyright (c) 2014 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


#
# Toolchain prefix
#
TC_PREFIX	:= msp430-elf-


#
# Directory containing toolchain executables
#
TC_BIN		:= $(TC_DIR)/bin


#
# Define toolchain variables
#
TC_AS		:= $(TC_PREFIX)as
TC_CC		:= $(TC_PREFIX)gcc
TC_AR		:= $(TC_PREFIX)ar
TC_LD		:= $(TC_PREFIX)gcc
TC_NM		:= $(TC_PREFIX)nm
TC_OBJDUMP	:= $(TC_PREFIX)objdump
TC_OBJCOPY	:= $(TC_PREFIX)objcopy
TC_READELF	:= $(TC_PREFIX)readelf
TC_SIZE		:= $(TC_PREFIX)size


#
# Detect toolchain information
#
TC_VERSION	:= $(shell $(TC_BIN)/$(TC_CC) -dumpversion)
TC_MACHINE	:= $(shell $(TC_BIN)/$(TC_CC) -dumpmachine)


#
# Toolchain specific search path for headers
#
TC_INCPATH	:= $(TC_DIR)/include


#
# Toolchain specific search path for libraries
#
# The linker scripts are located in the 'include" directory.
# Hence, it must be added to LIBPATH.
#
TC_LIBPATH	:= $(TC_DIR)/msp430-elf/lib $(TC_DIR)/include


#
# Toolchain specific compiler options
#
TC_CC_OPTS	:=


#
# Toolchain specific linker options
#
TC_LD_OPTS	:=
